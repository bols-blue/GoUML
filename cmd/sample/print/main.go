package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"os"

	"github.com/codegangsta/cli"
)

var (
	filterPortString string
	fileName         string
)
var globalFlags = []cli.Flag{
	cli.StringFlag{
		Name:        "filter_port",
		Value:       "8443:8883",
		Usage:       "MAC address based filtering port by immigration",
		Destination: &filterPortString,
		EnvVar:      "FILTER_PORT",
	},
	cli.StringFlag{
		Name:        "file",
		Value:       "main.go",
		Usage:       "",
		Destination: &fileName,
		EnvVar:      "FILENAME",
	},
}

func commandNotFound(c *cli.Context, command string) {
	fmt.Fprintf(os.Stderr, "%s: '%s' is not a %s command. See '%s --help'.", c.App.Name, command, c.App.Name, c.App.Name)
	os.Exit(127)
}

func main() {
	app := cli.NewApp()
	app.Name = "GoUML sample ast print"
	app.Usage = "print go ast"
	app.Version = "1.0"
	app.Flags = globalFlags
	app.Action = action
	app.CommandNotFound = commandNotFound

	err := app.Run(os.Args)
	if err != nil {
		os.Exit(1)
	}
}

func action(c *cli.Context) {
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}

	// Create the AST by parsing src.
	fset := token.NewFileSet() // positions are relative to fset
	f, err := parser.ParseFile(fset, "", content, 0)
	if err != nil {
		panic(err)
	}

	// Print the AST.
	ast.Print(fset, f)

}
