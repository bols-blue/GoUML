package main

import (
	"errors"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"os"
	"reflect"

	"github.com/codegangsta/cli"
)

func getPackageName(packageAlias ast.Node) (string, error) {
	if debug {
		// Print the AST.
		ast.Print(fset, packageAlias)
	}
	switch x := packageAlias.(type) {
	case *ast.Ident:
		return x.Name + ".", nil
	}
	return "", errors.New("getPackageName")
}

func printFunc(funcNode ast.Node) string {
	switch x := funcNode.(type) {
	case *ast.SelectorExpr:
		pName, err := getPackageName(x.X)
		if err != nil {
			return "funcCall "
		} else {
			return "funcCall " + pName + x.Sel.Name
		}
	case *ast.Ident:
		return "funcCall " + x.Name
	}
	if t := reflect.TypeOf(funcNode); t.Kind() == reflect.Ptr {
		return "funcCall " + "*" + t.Elem().Name()
	} else {
		return "funcCall (type)" + t.Name()
	}
}

var fset *token.FileSet

func action(c *cli.Context) {
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}

	// Create the AST by parsing src.
	fset = token.NewFileSet() // positions are relative to fset
	f, err := parser.ParseFile(fset, fileName, content, 0)
	if err != nil {
		panic(err)
	}

	// Inspect the AST and print all identifiers and literals.
	ast.Inspect(f, func(n ast.Node) bool {
		var s string
		switch x := n.(type) {
		case *ast.ImportSpec:
			s = "import " + x.Path.Value
			fmt.Printf("%s:\t%s\n", fset.Position(n.Pos()), s)
			return false
		case *ast.CallExpr:
			s = printFunc(x.Fun)
			fmt.Printf("%s:\t%s\n", fset.Position(n.Pos()), s)
			if debug {
				// Print the AST.
				ast.Print(fset, x)
			}
			return false
		case *ast.SelectorExpr:
			return false
		}
		if s != "" {
			fmt.Printf("%s:\t%s\n", fset.Position(n.Pos()), s)
		}
		return true
	})

}

var (
	filterPortString string
	fileName         string
	debug            bool
)
var globalFlags = []cli.Flag{
	cli.StringFlag{
		Name:        "filter_port",
		Value:       "8443:8883",
		Usage:       "MAC address based filtering port by immigration",
		Destination: &filterPortString,
		EnvVar:      "FILTER_PORT",
	},
	cli.StringFlag{
		Name:        "file",
		Value:       "main.go",
		Usage:       "",
		Destination: &fileName,
		EnvVar:      "FILENAME",
	},
	cli.BoolFlag{
		Name:        "debug",
		Usage:       "",
		Destination: &debug,
		EnvVar:      "DEBUG",
	},
}

func commandNotFound(c *cli.Context, command string) {
	fmt.Fprintf(os.Stderr, "%s: '%s' is not a %s command. See '%s --help'.", c.App.Name, command, c.App.Name, c.App.Name)
	os.Exit(127)
}

func main() {
	app := cli.NewApp()
	app.Name = "GoUML sample ast print"
	app.Usage = "print go ast"
	app.Version = "1.0"
	app.Flags = globalFlags
	app.Action = action
	app.CommandNotFound = commandNotFound

	err := app.Run(os.Args)
	if err != nil {
		os.Exit(1)
	}
}
